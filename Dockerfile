FROM node:19.0.0
WORKDIR /code
COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn
COPY . .
CMD [ "node" "app.js" ]