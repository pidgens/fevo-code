
const { nasaClient } = require('../clients')
const { getLastTenDays } = require('../utils/time')
const cache = require('../cache')

// every page has a limit of 25 items per page
// this could cause issues because we cannot do deeper filtering on the api request
// we can search by rover by using the cameras on the rovers
// https://api.nasa.gov
const getRover = async (ctx) => {
    const { camera } = ctx.request.query
    // create last 10 days
    const dates = getLastTenDays()
    // send last 10 day worth of responses
    const photoCollection = []
    let threePhotos = []
    for (let i = 0; i < dates.length; i++) {
        const date = dates[i]
        // check cache
        const cacheHit = cache.has(`${date}_${camera}`)
        if (cacheHit) {
            threePhotos = cache.get(`${date}_${camera}`)
        } else {
            let endpoint = `mars-photos/api/v1/rovers/curiosity/photos?earth_date=${date}&api_key=DEMO_KEY`
            if (camera) {
                endpoint += `&camera=${camera}`
            }
            const { data : { photos } } = await nasaClient.get(endpoint)
            threePhotos = photos.slice(0,3)
            cache.set(`${date}_${camera}`, threePhotos)
        }
        photoCollection.push(threePhotos)
    }
    ctx.body = photoCollection
}


module.exports = {
    getRover,
}