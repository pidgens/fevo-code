const http = require('http')
const koa = require('koa');
const koaRouter = require('koa-router');

const { getRover } = require('./api/pictures')

const setupApp = () => {
  const app = new koa();
  const router = new koaRouter();

  router.get('/', ctx => {
    ctx.body = 'K'
  })

  router.get('/health_check', (ctx) => {
    ctx.body = 'OK'
  })

  router.get('/rover', getRover)

  app
    .use(router.routes())
    .use(router.allowedMethods());

  return app
}

// const app = http.createServer( async (req, res) => {
//     if (req.url === '/rover' && req.method === 'GET') {
//         res.writeHead(200, { "Content-Type": "application/json" });
//         //set the response
//         const data = getRover()
//         //end the response
//         res.end();
//     } else {
//         res.writeHead(404, { "Content-Type": "application/json" });
//         res.end(JSON.stringify({ message: "Route not found" }));
//     }
// })

// app.listen(8080, () => {
//     console.log('start listening');
// })

module.exports = { setupApp }