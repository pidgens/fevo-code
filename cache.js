const NodeCache = require('node-cache');
const cache = new NodeCache({ stdTTL: 1000, checkperiod: 60*60*24 })

module.exports = cache