const server = require('./server')
const { setupApp } = require('./app')

const app = setupApp()
server(app, 8080)