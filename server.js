const { setupApp } = require('./app')

const server = (app, port) => {
    return app.listen(port, () => {
        console.log(`start listening to ${port}`)
    })
    
}

module.exports = server;