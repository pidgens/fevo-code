const supertest = require('supertest')
const createServer = require('../server')
const { setupApp } = require('./../app')

describe('Health Check', () => {
    let app;
    let server;
    let request;

    beforeEach(() => {
        app = setupApp()
        server = app.listen(3000)
        request = supertest(server)
        process.env.NASA_URL = 'https://api.nasa.gov'
    })
    afterEach(async () => {
        await server.close()
    })

    test('GET /health', (done) => {
        const res = request
            .get("/health_check")
            .expect((res) => {
                expect(res.status).toEqual(200)
                expect(res.text).toEqual('OK')
            })
            .end((err, res) => {
                if (err) return done(err);
                return done();
            });    
    })

    test('GET /rover', (done) => {
        process.env.NASA_URL = 'https://api.nasa.gov'
        const res = request
            .get('/rover?camera=NAVCAM')
            .expect((res) => {
                expect(res.status).toEqual(200)
                expect(JSON.parse(res.text).length).toEqual(10)
            })
            .end((err, res) => {
                if (err) return done(err);
                return done();
            });    
    })
})