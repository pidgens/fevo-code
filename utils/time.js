const moment = require('moment')

const getLastTenDays = () => {
    const dates = []
    for (let i = 0; i < 10; i++) {
        const date = moment()
        date.subtract(i, 'day')
        const formattedDate = moment(date).format('YYYY-MM-DD')
        dates.push(formattedDate)
    }   
    return dates;
}

module.exports = {
    getLastTenDays
}